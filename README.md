

## How to run the ToyWord2Vec


1. Install Python 3.6.7
2. python ToyWord2Vec.py
3. python word2VecSynonyms.py
4. That's it!

---

## What does the ToyWord2Vec.py do?


1. It reads and builds the training set from sensetest300.txt.
2. It generates word vectors of unit length.
3. For every word in the training set:
   2a. It moves vectors of neighboring words closer together.
   2b. It moves random pairs of vectors away from each other (negative sampling).
4. It iterates over step 3 the specified number of times.

---

## What does sensetest300.txt contain?

1. It is a toy corpus for training word vectors (or word embeddings).
2. It contains 557 unique words.
3. It has four pairs of words that occur in the exact same context (king, emperor), (quick, fast), (sea, ocean), (run, jog)

---

## What does word2VecSynonyms.py do?

1. It iterates through four target words - king, quick, sea, and run and finds the closest best match and prints it out.

---

## What does vecs.txt contain?

1. It contains the word vectors for 557 words in the training set.
2. This file is genrated when you run "python ToyWord2Vec.py"
3. This file is used by "word2VecSynonyms.py" to find the closest match for "king", "quick", "sea", and "run".