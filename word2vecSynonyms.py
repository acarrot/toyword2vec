import numpy as np

fp = open('vecs.txt', 'r')
lines = fp.readlines();
fp.close();

dictionary = {}

# read the word vectors
for line in lines:
    words = line.split(' ');

    vec = np.array([float(word) for word in words[1:len(words)-1]]);

    dictionary.update({words[0]: vec});

origwords = ['king', 'quick', 'sea', 'run'];

for origword in origwords:
    # get the word vector for the target word
    firstVec = dictionary[origword];
    print(origword);

    metric = -1
    bestkey = ''
    for key in dictionary.keys():

        localMetric = np.dot(firstVec, dictionary[key]) / (np.linalg.norm(firstVec)*np.linalg.norm(dictionary[key]));
        if (metric < localMetric and key != origword):
            metric = localMetric;
            bestkey = key;

    # print the best match
    print(bestkey, metric)
    print('------------------')
    print('')
    print('')

