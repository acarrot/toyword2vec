# -*- coding: utf-8 -*-
import random
import numpy as np
from collections import defaultdict

#textCorpus = ["natural language processing and machine learning is fun and exciting", "We had fun going to the zoo"];
#text = "natural language"

fp = open('sensetest300.txt', 'r');
textCorpus = fp.readlines();
fp.close();

print(len(textCorpus));

# Note the .lower() as upper and lowercase does not matter in our implementation
# [['natural', 'language', 'processing', 'and', 'machine', 'learning', 'is', 'fun', 'and', 'exciting']]
temp = []
for text in textCorpus:
    temp.append([word.lower() for word in text.split()])

textCorpus = temp;

settings = {
	'window_size': 2,	# context window +- center word
	'n': 30,		# dimensions of word embeddings matrix
	'epochs': 100,		# number of training epochs
	'learning_rate': 0.01,	# learning rate
	'neg_samples': 30,
	'threshold':  0		# threshold for deciding what words to keep in training data
}

# build training data
def generate_training_data(settings, textCorpus):
    # Find unique word counts using dictonary
    word_counts = defaultdict(int)
    for row in textCorpus:
        for word in row:
            word = word.rstrip('\n');
            word_counts[word] += 1

    ## How many unique words in vocab? 9
    v_count = len(word_counts.keys())
    word_counts = {key: val for key, val in word_counts.items() if val > settings['threshold']};
    word_counts = defaultdict(int, word_counts);

    # Generate Lookup Dictionaries (vocab)
    words_list = list(word_counts.keys())
    # Generate word:index
    word_index = dict((word, i) for i, word in enumerate(words_list))
    # Generate index:word
    index_word = dict((i, word) for i, word in enumerate(words_list))

    v_count = len(word_counts.keys())
    print('VOCAB_SIZE ', v_count);

    text_training_data = []
    # Cycle through each sentence in textCorpus
    for sentence in textCorpus:
        sent_len = len(sentence)
        # Cycle through each word in sentence
        for i, word in enumerate(sentence):
            word = word.rstrip('\n');
            if (sentence[i] not in word_index.keys()):
                continue;

            w_target = word_index[sentence[i]]
            # Cycle through context window
            w_left_context = []
            w_right_context = []
            # Note: window_size 2 will have range of 5 values
            for j in range(i - settings['window_size'], i):
                if j < 0:
                    w_left_context.append(-1);
                else:
                    if (sentence[j] in word_index.keys()):
                        w_left_context.append(word_index[sentence[j]]);
                    else:
                        w_left_context.append(-1);

            for j in range(i + 1, i + settings['window_size']+1):
                if j > sent_len-1:
                    w_right_context.append(-1);
                else:
                    if (sentence[j] in word_index.keys()):
                        w_right_context.append(word_index[sentence[j]])
                    else:
                        w_right_context.append(-1);
            text_training_data.append([w_target, w_left_context, w_right_context])

    return v_count, np.array(text_training_data), word_index, index_word

# Numpy ndarray with one-hot representation for [target_word, context_words]
vocab_size, text_training_data, word_index, index_word = generate_training_data(settings, textCorpus)

NUM_VECS = vocab_size;
SIZE = settings['n']; 

vecsA =  []
for i in range(NUM_VECS):
    vecsA.append(np.random.rand(SIZE));

for t in range(settings['epochs']):

    loss = 0;
    loss1 = 0;
    for w_t, w_l_c, w_r_c in text_training_data: 
        y_t = vecsA[w_t];

        for w_l_c_i in w_l_c:
            if (w_l_c_i >= 0):
                loss += np.sum(np.square(vecsA[w_l_c_i] - vecsA[w_t]));

                vecsA[w_t] += settings['learning_rate']*(vecsA[w_l_c_i] - vecsA[w_t]);
                vecsA[w_l_c_i] += settings['learning_rate']*(vecsA[w_t] - vecsA[w_l_c_i]);

                vecsA[w_t] /= np.linalg.norm(vecsA[w_t]);
                vecsA[w_l_c_i] /= np.linalg.norm(vecsA[w_l_c_i]);

        for w_r_c_i in w_r_c:
            if (w_r_c_i >= 0):
                loss += np.sum(np.square(vecsA[w_r_c_i] - vecsA[w_t]));

                vecsA[w_t] += settings['learning_rate']*(vecsA[w_r_c_i] - vecsA[w_t]);
                vecsA[w_r_c_i] += settings['learning_rate']*(vecsA[w_t] - vecsA[w_r_c_i]);

                vecsA[w_t] /= np.linalg.norm(vecsA[w_t]);
                vecsA[w_r_c_i] /= np.linalg.norm(vecsA[w_r_c_i]);

        for l in range(settings['neg_samples']):
            i = random.randint(0, NUM_VECS - 1);
            j = random.randint(0, NUM_VECS - 1 );
            while (i == j):
                j = random.randint(0, NUM_VECS - 1);

            localLoss = np.sum(np.square(vecsA[i] - vecsA[j]));

            vecsA[i] += settings['learning_rate']*(vecsA[i] - vecsA[j]);
            vecsA[j] += settings['learning_rate']*(vecsA[j] - vecsA[i]);

            vecsA[i] /= np.linalg.norm(vecsA[i]);
            vecsA[j] /= np.linalg.norm(vecsA[j]);

            loss1 += -localLoss;

    # THE FOLLOWING HAS TO BE CHANGED TO PRINT THE RIGHT KIND OF LOSS
    # MOST PROBABLY SHOULD HAVE THREE KINDS OF LOSS
    if t % np.floor(settings['epochs']/100) == np.floor(settings['epochs']/100) - 1:
        print(t, loss)
        print(t, loss1)

        print('')
        print('')
        print('')


fp = open('vecs.txt', 'w');
for i in range(NUM_VECS):
    fp.write(index_word[i]+' ');

    for j in range(settings['n']):
        fp.write(str(vecsA[i][j])+' ');

    fp.write('\n');
fp.close();
